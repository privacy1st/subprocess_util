#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from pathlib import Path

from subprocess_util.chunked_transfer.active_send.exec_send_chunks_active import exec_send_chunks_active


def main():
    args = parse_args()
    #
    ssh_target = args.ssh_target
    chunk_size = args.chunk_size
    compressed_data: bool = args.compressed_data
    child: Path = args.child
    parent: Path | None = args.parent
    source_chunk_dir: Path = args.chunk_dir
    target_chunk_dir: Path = args.target_chunk_dir

    command_parts = (
        ['btrfs', 'send'],
        ['-p', str(parent)] if parent else [],
        ['--compress-data'] if compressed_data else [],
        [str(child)]
    )
    command = [x for xs in command_parts for x in xs]

    returncode = exec_send_chunks_active(
        command,
        ssh_target,
        source_chunk_dir,
        target_chunk_dir,
        chunk_size
    )
    exit(returncode)


def parse_args():
    parser = argparse.ArgumentParser(prog='btrfs-send-chunks-active')

    parser.add_argument('-p',
                        help='Path to parent subvolume of CHILD_SUBVOLUME. '
                             'If given, '
                             'only the difference between PARENT_SUBVOLUME and CHILD_SUBVOLUME is transferred.',
                        dest='parent',
                        default=None,
                        type=Path,
                        metavar='PARENT_SUBVOLUME'
                        )

    parser.add_argument('--compressed-data',
                        help='Reduce network bandwidth by compressing data.',
                        dest='compressed_data',
                        action='store_true',
                        default=False,
                        )

    parser.add_argument('--chunk-size',
                        help='Size in bytes. Defaults to 128 MB.',
                        dest='chunk_size',
                        type=int,
                        default=128 * 1024 * 1024,
                        )

    parser.add_argument('--chunk-dir',
                        help='Chunks are saved in this directory. '
                             'Defaults to directory `<CHILD_SUBVOLUME>.chunk` next to CHILD_SUBVOLUME.',
                        dest='chunk_dir',
                        type=Path,
                        metavar='CHUNK_DIR',
                        default=None,
                        )

    parser.add_argument('--target-chunk-dir',
                        help='Chunks are saved in this directory on the receiving side. '
                             'Must be an absolute path. '
                             'Defaults to directory `<TARGET_SUBVOLUME>.chunk` next to TARGET_SUBVOLUME.',
                        dest='target_chunk_dir',
                        type=Path,
                        metavar='TARGET_CHUNK_DIR',
                        default=None,
                        )

    parser.add_argument('child',
                        help='Path to child subvolume. '
                             'This subvolume gets replicated at TARGET_SUBVOLUME on the receiving side.',
                        type=Path,
                        metavar='CHILD_SUBVOLUME'
                        )

    parser.add_argument('ssh_target',
                        help='Hostname of receiving side as configured in ~/.ssh/config.',
                        metavar='SSH_TARGET'
                        )

    parser.add_argument('target_path',
                        help='Absolute path to TARGET_SUBVOLUME on the receiving side.',
                        type=Path,
                        metavar='TARGET_SUBVOLUME'
                        )

    args = parser.parse_args()

    # child
    args.child = args.child.absolute()

    # target_path
    if not args.target_path.is_absolute():
        raise ValueError('TARGET_SUBVOLUME must be absolute')

    # chunk_dir
    if args.chunk_dir is None:
        args.chunk_dir = args.child.parent.joinpath(f'{args.child.name}.chunk')
    else:
        args.chunk_dir = args.chunk_dir.absolute()

    # target_chunk_dir
    if args.target_chunk_dir is None:
        args.target_chunk_dir = args.target_path.parent.joinpath(f'{args.target_path.name}.chunk')
    else:
        if not args.target_chunk_dir.is_absolute():
            raise ValueError('TARGET_CHUNK_DIR must be absolute')

    # parent
    if args.parent is not None:
        args.parent = args.parent.absolute()

    return args


if __name__ == '__main__':
    main()
