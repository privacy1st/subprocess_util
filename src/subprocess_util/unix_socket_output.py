import socket
from pathlib import Path


def write_message(socket_file: Path,
                  message: bytes):
    """
    Writes `message` to the UNIX socket `socket_file`.

    :param socket_file:
    :param message:
    :return:
    """

    # INSPIRATION: https://pymotw.com/3/socket/uds.html

    print(f'Connecting to socket {socket_file}')
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.connect(str(socket_file))
    try:
        sock.sendall(message)
    finally:
        print(f'Closing socket {socket_file}')
        sock.close()
