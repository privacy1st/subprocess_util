# subprocess-util

## Example to replicate a BTRFS subvolume with an active sending side

The sending side "pushes" the BTRFS snapshot to the receiving side. The sender needs password-less SSH access to the receiver.

First, start the receiving side:

```shell
sudo btrfs-receive-chunks-passive \
  /target-snapshot-path/20230213T1809_u/
```

Then, start the sending side:

```shell
sudo btrfs-send-chunks-active \
  /source-snapshot-path/20230213T1809_u/ \
  ssh-hostname-of-receiving-side \
  /target-snapshot-path/20230213T1809_u/
```

See [test16()](src/subprocess_util/test.py).

## Example to replicate a BTRFS subvolume with an active receiving side

The receiving side "pulls" the BTRFS snapshot from the sending side. The receiver needs password-less SSH access to the sender.

See [test17()](src/subprocess_util/test.py).
