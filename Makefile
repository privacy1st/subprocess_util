.ONESHELL:
SHELL := bash
# https://github.com/JordanMartinez/purescript-cookbook/blob/master/makefile
# set -e = bash immediately exits if any command has a non-zero exit status.
# set -u = a reference to any shell variable you haven't previously
#    defined -- with the exceptions of $* and $@ -- is an error, and causes
#    the program to immediately exit with non-zero code.
# set -o pipefail = the first non-zero exit code emitted in one part of a
#    pipeline (e.g. `cat file.txt | grep 'foo'`) will be used as the exit
#    code for the entire pipeline. If all exit codes of a pipeline are zero,
#    the pipeline will emit an exit code of 0.
.SHELLFLAGS := -eu -o pipefail -c

.PHONY: all
all: install

.PHONY: install
install:
	sudo python3 -m pip install --upgrade --force-reinstall .

.PHONY: test
test: venv
	source venv/bin/activate
	export PYTHONPATH="${PYTHONPATH}:src"

	python3 src/subprocess_util/test.py

.PHONY: venv
venv:
	if [ ! -d venv ]; then
	  	python3 -m venv venv
	fi

.PHONY: clean
clean:
	rm -rf venv .mypy_cache build dist src/__pycache__ src/subprocess_util.egg-info test
