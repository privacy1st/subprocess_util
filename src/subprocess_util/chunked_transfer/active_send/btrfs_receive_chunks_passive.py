#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from pathlib import Path

from subprocess_util.chunked_transfer.active_send.exec_receive_chunks_passive import exec_receive_chunks_passive


def main():
    args = parse_args()
    #
    target_chunk_dir: Path = args.chunk_dir
    target_path: Path = args.target_path

    command = ['btrfs', 'receive', str(target_path.parent)]

    returncode = exec_receive_chunks_passive(
        command,
        target_chunk_dir,
    )
    exit(returncode)


def parse_args():
    parser = argparse.ArgumentParser(prog='btrfs-receive-chunks-passive')

    parser.add_argument('--chunk-dir',
                        help='Chunks are saved in this directory. '
                             'Defaults to directory `<TARGET_SUBVOLUME>.chunk` next to TARGET_SUBVOLUME.',
                        dest='chunk_dir',
                        type=Path,
                        metavar='CHUNK_DIR',
                        default=None,
                        )

    parser.add_argument('target_path',
                        help='Path where the subvolume will be created. '
                             'The last component of the path '
                             'must be equal to the name of CHILD_SUBVOLUME on the sending side.',
                        type=Path,
                        metavar='TARGET_SUBVOLUME'
                        )

    args = parser.parse_args()

    # target_path
    args.target_path = args.target_path.absolute()

    # chunk_dir
    if args.chunk_dir is None:
        args.chunk_dir = args.target_path.parent.joinpath(f'{args.target_path.name}.chunk')
    else:
        args.chunk_dir = args.chunk_dir.absolute()

    return args


if __name__ == '__main__':
    main()
