#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import socket
from pathlib import Path


def wait_for_message(socket_file: Path,
                     messages: list[bytes]) -> bytes:
    """
    Creates a UNIX socket at `socket_file`.

    Accepts connections on the UNIX socket,
    until a client sends one of the given `messages`.

    Closes the UNIX socket.

    :return: The message that was received.
    """

    # INSPIRATION: https://pymotw.com/3/socket/uds.html

    print(f'Listening on socket {socket_file}')
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.bind(str(socket_file))
    sock.listen(1)

    command = accept_loop_until_message(sock, messages)

    print(f'Closing socket {socket_file}')
    sock.close()
    socket_file.unlink(missing_ok=False)

    return command


def accept_loop_until_message(sock: socket.socket,
                              messages: list[bytes]) -> bytes:
    """
    Uses an open UNIX socket.

    Accepts connections on the UNIX socket,
    until one client sends one of the given commands as first message.

    Does not close the UNIX socket.

    :returns: The command that was received.
    """

    bufsize = max([len(msg) for msg in messages]) + len(b'\n')

    while True:
        connection, client_address = sock.accept()
        try:
            b: bytes = connection.recv(bufsize)
            for msg in messages:
                if b == msg:
                    print(f'Received "{msg}".')
                    return msg
            print(f'Received unknown message: {b}')
        except Exception as e:
            print(f'Error while reading message: {e}')
        finally:
            connection.close()
