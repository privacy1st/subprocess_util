#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import socket
from pathlib import Path
from typing import IO, AnyStr, Callable

from subprocess_util.unix_sock_input import accept_loop_until_message


def receive_inform(in_pipe: IO[AnyStr],
                   socket_file: Path,
                   chunk_file_tmpl: Path,
                   get_chunk_file: Callable[[Path, int], Path],
                   ) -> None:
    """
    Creates a listening UNIX socket at `socket_file`.

    Waits for incoming messages `OK` and `EOF` on the socket.
    
    Once a message is received,
    the file returned by `get_chunk_file(chunk_file_tmpl, chunk_no)`
    is read and then written to `in_pipe`.
    
    If the received message was `EOF`, then the listening loop ends.
    If the received message was `OK`, then chunk_no is increased by one and the loop continues.
    
    Finally, the socket is closed.
    
    :param get_chunk_file:
    :param in_pipe:
    :param chunk_file_tmpl:
    :param socket_file: Create a UNIX socket and wait for messages.
    :return:
    """
    print(f'Listening on socket {socket_file}')
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.bind(str(socket_file))
    sock.listen(1)

    ct = 1
    messages = [b'OK\n', b'EOF\n']
    while True:
        msg = accept_loop_until_message(sock, messages)
        if msg not in messages:
            raise ValueError("Invalid state")

        chunk_file = get_chunk_file(chunk_file_tmpl, ct)
        chunk = chunk_file.read_bytes()
        in_pipe.write(chunk)
        # in_pipe.flush()  # TODO: is this required?
        chunk_file.unlink(missing_ok=False)

        if msg == b'OK\n':
            ct += 1
        elif msg == b'EOF\n':
            break
        else:
            raise ValueError("Invalid state")

    print(f'Closing socket {socket_file}')
    sock.close()
    socket_file.unlink(missing_ok=False)

    in_pipe.flush()

    # TODO: Has this any effect? On stdin probably yes!
    in_pipe.close()
