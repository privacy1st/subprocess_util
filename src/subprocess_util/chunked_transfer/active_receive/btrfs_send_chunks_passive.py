#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from pathlib import Path

from subprocess_util.chunked_transfer.active_receive.exec_send_chunks_passive import exec_send_chunks_passive


def main():
    args = parse_args()
    #
    chunk_size = args.chunk_size
    compressed_data: bool = args.compressed_data
    child: Path = args.child
    parent: Path | None = args.parent
    source_chunk_dir: Path = args.chunk_dir

    command_parts = (
        ['btrfs', 'send'],
        ['-p', str(parent)] if parent else [],
        ['--compress-data'] if compressed_data else [],
        [str(child)]
    )
    command = [x for xs in command_parts for x in xs]
    returncode = exec_send_chunks_passive(
        command,
        source_chunk_dir,
        chunk_size,
    )
    exit(returncode)


def parse_args():
    parser = argparse.ArgumentParser(prog='btrfs-send-chunks-passive')

    parser.add_argument('-p',
                        help='Path to parent subvolume.',
                        dest='parent',
                        default=None,
                        type=Path,
                        metavar='PARENT_SUBVOLUME'
                        )

    parser.add_argument('--compressed-data',
                        help='Reduce network bandwidth by compressing data.',
                        dest='compressed_data',
                        action='store_true',
                        default=False,
                        )

    parser.add_argument('--chunk-size',
                        help='Size in bytes. Defaults to 128 MB.',
                        dest='chunk_size',
                        type=int,
                        default=128 * 1024 * 1024,
                        )

    parser.add_argument('--chunk-dir',
                        help='Chunks are saved in this directory. '
                             'Defaults to directory `<CHILD_SUBVOLUME>.chunk` next to CHILD_SUBVOLUME.',
                        dest='chunk_dir',
                        type=Path,
                        metavar='CHUNK_DIR',
                        default=None,
                        )

    parser.add_argument('child',
                        help='Path to child subvolume.',
                        type=Path,
                        metavar='CHILD_SUBVOLUME'
                        )

    args = parser.parse_args()

    # child
    args.child = args.child.absolute()

    # chunk_dir
    if args.chunk_dir is None:
        args.chunk_dir = args.child.parent.joinpath(f'{args.child.name}.chunk')
    else:
        args.chunk_dir = args.chunk_dir.absolute()

    # parent
    if args.parent is not None:
        args.parent = args.parent.absolute()

    return args


if __name__ == '__main__':
    main()
