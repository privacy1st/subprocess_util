#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from pathlib import Path

from subprocess_util.chunked_transfer.active_receive.exec_receive_chunks_active import exec_receive_chunks_active


def main():
    args = parse_args()
    #
    ssh_source: str = args.ssh_source
    source_chunk_dir: Path = args.source_chunk_dir
    target_chunk_dir: Path = args.chunk_dir
    target_path: Path = args.target_path

    command = ['btrfs', 'receive', str(target_path.parent)]

    returncode = exec_receive_chunks_active(
        command,
        ssh_source,
        source_chunk_dir,
        target_chunk_dir,
    )
    exit(returncode)


def parse_args():
    parser = argparse.ArgumentParser(prog='btrfs-receive-chunks-active',
                                     description='Either CHILD_SUBVOLUME or SOURCE_CHUNK_DIR must be given')

    parser.add_argument('--chunk-dir',
                        help='Chunks are saved in this directory. '
                             'Defaults to directory `<TARGET_SUBVOLUME>.chunk` next to TARGET_SUBVOLUME.',
                        dest='chunk_dir',
                        type=Path,
                        metavar='CHUNK_DIR',
                        default=None,
                        )

    parser.add_argument('ssh_source',
                        help='Hostname of sending side as configured in ~/.ssh/config.',
                        metavar='SSH_SOURCE'
                        )

    parser.add_argument('--child',
                        help='Path to child subvolume on sending side. '
                             'Must be an absolute path.',
                        dest='child',
                        type=Path,
                        metavar='CHILD_SUBVOLUME',
                        default=None,
                        )

    parser.add_argument('--source-chunk-dir',
                        help='Chunks are saved in this directory on sending side. '
                             'Must be an absolute path. '
                             'Defaults to directory `<CHILD_SUBVOLUME>.chunk` next to CHILD_SUBVOLUME',
                        dest='source_chunk_dir',
                        type=Path,
                        metavar='SOURCE_CHUNK_DIR',
                        default=None,
                        )

    parser.add_argument('target_path',
                        help='Path where CHILD_SUBVOLUME will be replicated. '
                             'The last component of the path '
                             'must be equal to the name of CHILD_SUBVOLUME on the sending side.',
                        type=Path,
                        metavar='TARGET_SUBVOLUME'
                        )

    args = parser.parse_args()

    # child and source_chunk_dir
    if args.child is None and args.source_chunk_dir is None:
        raise ValueError('Either CHILD_SUBVOLUME or SOURCE_CHUNK_DIR must be given')

    if args.source_chunk_dir is not None:
        if not args.source_chunk_dir.is_absolute():
            raise ValueError('SOURCE_CHUNK_DIR must be absolute')
    if args.child is not None:
        if not args.child.is_absolute():
            raise ValueError('CHILD_SUBVOLUME must be absolute')

    if args.source_chunk_dir is None:
        args.source_chunk_dir = args.child.parent.joinpath(f'{args.child.name}.chunk')

    # target_path
    args.target_path = args.target_path.absolute()

    # chunk_dir
    if args.chunk_dir is None:
        args.chunk_dir = args.target_path.parent.joinpath(f'{args.target_path.name}.chunk')
    args.chunk_dir = args.chunk_dir.absolute()

    return args


if __name__ == '__main__':
    main()
