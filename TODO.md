# TODOs

* Create a separate module `btrfs_util` and btrfs specific .py files there:
  * btrfs_send_{active/passive}
  * btrfs_receive{active/passive}
  * see also: setup.cfg > console-scripts

* print total transfer speed and last chunk transfer speed

* create script entry points similar to `cat` and `tee` that are independent of the sending and receiving program (btrfs send/receive) in our case:
  * sending side: `btrfs send | send-chunks-active root@target`
  * receiving side: `receive-chunks-passive | btrfs receive`
